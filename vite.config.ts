import { sveltekit } from '@sveltejs/kit/vite';
import { resolve } from 'path'
import type { UserConfig } from 'vite';

const config: UserConfig = {
	plugins: [sveltekit()],
};

export default config;
