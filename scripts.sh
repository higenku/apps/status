# Get all the checks
Results= 
  curl -X 'GET' \
    'https://api.checklyhq.com/v1/check-statuses' \
    -H 'accept: application/json' \
    -H 'x-checkly-account: 6c8eb467-19bd-4538-a7fe-ae71edb557de' \
    -H 'Authorization: Bearer cu_f733624661c14bdd9180bbb727bdc204'

  # {
  #   "name": "XAccounts Api",
  #   "hasErrors": false,
  #   "hasFailures": false, <- isUP
  #   "longestRun": 19072,
  #   "shortestRun": 3,
  #   "checkId": "28013681-ba57-4a79-b452-b9d074b88e92", <- Used down
  #   "created_at": "2022-04-09T15:24:06.171Z",
  #   "updated_at": "2023-01-25T14:09:06.952Z",
  #   "lastRunLocation": "us-east-2",
  #   "lastCheckRunId": "1674655746122",
  #   "sslDaysRemaining": 155,
  #   "isDegraded": false
  # },
  # {
  #     "name": "Database",
  #     "hasErrors": false,
  #     "hasFailures": true, <- isNotUp
  #     "longestRun": 12317,
  #     "shortestRun": 4,
  #     "checkId": "661e0171-8458-4f13-8b92-3d20315a1700",
  #     "created_at": "2022-04-06T13:02:59.527Z",
  #     "updated_at": "2023-01-25T14:20:55.231Z",
  #     "lastRunLocation": "eu-central-1",
  #     "lastCheckRunId": "1674656449894",
  #     "sslDaysRemaining": 154,
  #     "isDegraded": false
  # },

for checkId in Results; do 
  # Get status from the last 7 days
  curl -X 'GET' \
    'https://api.checklyhq.com/v1/analytics/api-checks/661e0171-8458-4f13-8b92-3d20315a1700?quickRange=last7Days&metrics=responseTime&limit=10&page=1' \
    -H 'accept: application/json' \
    -H 'x-checkly-account: 6c8eb467-19bd-4538-a7fe-ae71edb557de' \
    -H 'Authorization: Bearer cu_f733624661c14bdd9180bbb727bdc204'
  # {
  #   "activated" : true,
  #   "checkId" : "661e0171-8458-4f13-8b92-3d20315a1700",
  #   "checkType" : "API",
  #   "frequency" : 30,
  #   "from" : "2023-01-18T14:23:08.124Z",
  #   "metadata" : {
  #       "availability" : {
  #         "label" : "Availability",
  #         "unit" : "percentage"
  #       }
  #   },
  #   "muted" : false,
  #   "name" : "Database",
  #   "pagination" : {
  #       "limit" : 10,
  #       "page" : 1
  #   },
  #   "series" : [
  #       {
  #         "data" : [
  #             {
  #               "availability" : 90.763,
  #               "success" : 845,
  #               "total" : 931
  #             }
  #         ]
  #       }
  #   ],
  #   "tags" : [],
  #   "to" : "2023-01-25T14:23:08.124Z"
  # }



  # Get availability
  curl -X 'GET' \
    'https://api.checklyhq.com/v1/analytics/api-checks/661e0171-8458-4f13-8b92-3d20315a1700?quickRange=last7Days&metrics=availability&limit=10&page=1' \
    -H 'accept: application/json' \
    -H 'x-checkly-account: 6c8eb467-19bd-4538-a7fe-ae71edb557de' \
    -H 'Authorization: Bearer cu_f733624661c14bdd9180bbb727bdc204'
  # {
  #   "activated" : true,
  #   "checkId" : "661e0171-8458-4f13-8b92-3d20315a1700",
  #   "checkType" : "API",
  #   "frequency" : 30,
  #   "from" : "2023-01-18T14:24:02.599Z",
  #   "metadata" : {
  #       "availability" : {
  #         "label" : "Availability",
  #         "unit" : "percentage"
  #       }
  #   },
  #   "muted" : false,
  #   "name" : "Database",
  #   "pagination" : {
  #       "limit" : 10,
  #       "page" : 1
  #   },
  #   "series" : [
  #       {
  #         "data" : [
  #             {
  #               "availability" : 90.753,
  #               "success" : 844,
  #               "total" : 930
  #             }
  #         ]
  #       }
  #   ],
  #   "tags" : [],
  #   "to" : "2023-01-25T14:24:02.599Z"
  # }

done

