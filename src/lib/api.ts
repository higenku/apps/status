import { json } from "@sveltejs/kit";

const AUTH_TOKEN = import.meta.env.VITE_AUTH_TOKEN;
const CHECKLY_AC = import.meta.env.VITE_CHECKLY_AC;

export const api_request = async (path: string) =>{
	if (typeof caches !== 'undefined') {
		const cache = await caches.open('check-statuses')
		let req = await cache.match(`https://api.checklyhq.com/v1/${path}`);
		if (typeof req === 'undefined') {
			console.log('Putting stuff in the cache')
			let response = await fetch(`https://api.checklyhq.com/v1/${path}`, {
				// @ts-ignore
			
				cf: {
					cacheTtl: 86400 / 2,
					cacheEverything: true
				},
				headers: {
					accept: 'application/json',
					'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0',
					'x-checkly-account': CHECKLY_AC,
					Authorization: `Bearer ${AUTH_TOKEN}`,
				}
			});
			
			let type = response.headers.get("Content-Type");
			const bodyJson = await response.json()

			response = new Response(json(bodyJson).body, {
				// @ts-ignore
				headers: {
					"Content-Type": type,
					"Access-Control-Allow-Origin": "*",
					"Cache-Control": "public, max-age=10800"
				}
			});
			await cache.put(`https://api.checklyhq.com/v1/${path}`, response)

			return json(bodyJson)
		}
		console.log('Using cache')
		
		return req
	}
	console.log('Using something outside the cache')

	return await fetch(`https://api.checklyhq.com/v1/${path}`, {
		headers: {
			accept: 'application/json',
			'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0',
			'x-checkly-account': CHECKLY_AC,
			Authorization: `Bearer ${AUTH_TOKEN}`
		}
	});
}
