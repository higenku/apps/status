import {json, type RequestHandler} from "@sveltejs/kit"
import {api_request} from "$~/lib"


export const GET : RequestHandler = async (event) => {
    const res = api_request('check-statuses')
    return json(await (await res).json())
}